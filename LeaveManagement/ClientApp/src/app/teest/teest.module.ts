import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeestRoutingModule } from './teest-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TeestRoutingModule
  ]
})
export class TeestModule { }
